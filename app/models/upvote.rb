class Upvote < ApplicationRecord
  belongs_to :user  #Appartient a ...
  belongs_to :product
  validates :user, uniqueness: { scope: :product }
end
