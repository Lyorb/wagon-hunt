# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#TODO : Destroy by hand (rails db:drop)
#Product.destroy_all
#User.destroy_all

lior = User.create!(email: 'liorbes@gmail.com', password: 'besnainou')
lea = User.create!(email: 'lea125@gmail.com', password: 'besnainou')

kudoz = Product.create!(user: lior, name: "Kudoz", url: "http://getudoz.com", tagline: "Tinder for job search", category: "tech")
Product.create!(user: lea, name: "uSlide", url: "http://Uslide.com", tagline: "youtube suck for education", category: "education")
airbnb = Product.create!(user: lior, name: "AirBnb", url: "http://airbnb.com", tagline: "Not visiting, live", category: "education")


#Upvotes

kudoz.upvotes.create! user: lior
kudoz.upvotes.create! user: lea

airbnb.upvotes.create! user: lior
